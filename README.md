# WebSupport Shell Autoconnector

This software allows to automatically access the WebSupport WebAdmin to activate a shell and read connection credentials.

## Usage

1. Clone the repo to some directory
2. Install it using `yarn`

`./wsh.sh example.com username password` where `username` and `password` are your login credentials to a WebSupport account with access to `example.com`.

__Note: With this method you must have ssh key submited for the selected domain__

## Setup globally

Prefered way is to set enviroment variables and an alias
```
export WS_USERNAME="username"
export WS_PASSWORD="password"
alias wsh='(pushd /path/to/installed/package && ./wsh.sh; popd)'
```


## API

WebSupport credentials can be submitted either by arguments or by enviroment variables `WS_USERNAME` and `WS_PASSWORD`.
As we use new ES7 features, you need to run the standalone script using babel.


Then the following command
`babel-node . example.com username password`
will return a string `ssh_user ssh_password ssh_host ssh_port` where individual placeholders are ssh credentials.

## BitBucket Pipelines

Read the ssh credentials into variables:
`read ssh_user ssh_password ssh_host ssh_port <<<$(babel-node . example.com $WS_USERNAME $WS_PASSWORD) ;`

Use `sshpass` utility to use password from the CLI. 

`sshpass -p $ssh_password ssh -o StrictHostKeyChecking=no $ssh_user@$ssh_host -p$ssh_port ./deploy-$BITBUCKET_REPO_SLUG.sh`

__(Works if we use `~/deploy-repo-slug.sh` convention)__
