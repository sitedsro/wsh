import fetch from 'node-fetch'
import cheerio from 'cheerio'
import {join, fromPairs} from 'lodash'
import formURLEncoded from 'form-urlencoded'


const API_DEBUG = false
const ROUTE_LOGIN = 'https://admin.websupport.sk/sk/site/login'
const ROUTE_SHELL = (domain) => `https://admin.websupport.sk/sk/hosting/shell?domain=${domain}`

let cookies
let YII_CSRF_TOKEN

const env = (key, defaultValue) => {
  if (process.env[key] !== undefined) {
    return process.env[key]
  } else if (defaultValue !== undefined) {
    return defaultValue
  } else {
    throw new Error(`Undefined environment variable: ${key}`)
  }
}

const request = async (method, path, options = {}) => {
	const hasBody = !['HEAD', 'GET'].includes(method.toUpperCase())
  	const body = hasBody ? formURLEncoded(options.body) : undefined
  	const typeHeaders = hasBody ? {'Content-Type': 'application/x-www-form-urlencoded'} : {}
	const headers = {
			...typeHeaders,
			'Cookie': join(cookies, ';'),
			...options.headers,
	}
	const url = !API_DEBUG ? path : 'http://httpbin.org/anything'
	return fetch(url, {credentials: 'include', method, headers, body})
		.then((res) => {
			cookies = cookies || res.headers.getAll('set-cookie')
			return res.text()
		})
}

const login = async (username, password) => {
	const $ = cheerio.load(await request('GET', ROUTE_LOGIN))
	YII_CSRF_TOKEN = $('input[name=YII_CSRF_TOKEN]').val()

	return cheerio.load(await request('POST', ROUTE_LOGIN, {body: {
		'LoginForm[username]' : username,
		'LoginForm[password]' : password,
		YII_CSRF_TOKEN,
	}}))
}

const isShellActive = ($) => $('#yw0 button[type=submit]').text().trim() !== 'Aktivovať prístup'

const getActiveShell = async (domain) => {
	let $ = cheerio.load(await request('GET', ROUTE_SHELL(domain)))
	if (!isShellActive($)) {
		$ = cheerio.load(await request('POST', ROUTE_SHELL(domain), {body: {
			'access' : 'shell-activate',
			YII_CSRF_TOKEN,
		}}))
	}
	return $
}

const findCredentials = ($) => fromPairs($('.form-group').map(function(index, element) {
		return [[$(element).find('label').text().trim(), $(element).find('div').text().trim()]]
	}).get())

const getShellAccess = async (username, password, domain) => {
	await login(username, password)
	const credentials = findCredentials(await getActiveShell(domain))
	return [
		credentials['Prihlasovacie meno:'],
		credentials['Heslo:'],
		credentials['SSH host:'],
		credentials['SSH port:'],
	]
}


try {
	const username = process.argv[3] || env('WS_USERNAME')
	const password = process.argv[4] || env('WS_PASSWORD')
	const domain = process.argv[2] || 'sited.sk'
	if (!(username && password && domain)) throw new Error('You have to provide credentials.')
	getShellAccess(username, password, domain)
		.then((ssh) => process.stdout.write(ssh.join(' ')))
} catch (e) {
	console.error('Error happened', e)
}
